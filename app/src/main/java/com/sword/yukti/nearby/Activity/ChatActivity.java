package com.sword.yukti.nearby.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.sword.yukti.nearby.ADapter.MessageListAdapter;
import com.sword.yukti.nearby.ADapter.Record_adapter_class;
import com.sword.yukti.nearby.ContactActivity;
import com.sword.yukti.nearby.R;
import com.sword.yukti.nearby.Utility.Getter_setter;
import com.sword.yukti.nearby.Utility.isRead_gettersetter;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {
    Long timestamp, swipetimestamp;
    String date, old_date, yourDate;
    FirebaseUser curr_user;
    ArrayList<Getter_setter> chat_list;
    String key_value, TodaysDate, YesterdayDate;
    String mess, lastMsg, lastMsgTime, statusofSendMsg;
    String send, received, send_time, received_time;
    Record_adapter_class adapter;
    ListView listView;
    EditText editText;
    ImageButton send_mess;
    Calendar c, c2;
    SimpleDateFormat currentdate, backdate;
    int hoursToSubtract = 24, i = 0, jj = 0;
    String user_email, user_login, name = "", login_userName;
    TextView name_textview;
    Boolean aBoolean = true;
    String server_timestamp;
    SwipeRefreshLayout swipeRefreshLayout;
    DatabaseReference databaseReference, databaseReference2;
    ChildEventListener childEventListener;
    private ListView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mMessageRecycler = (ListView) findViewById(R.id.reyclerview_message_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        String title_name = getIntent().getStringExtra("name");
        name = getIntent().getStringExtra("name");
        user_email = getIntent().getStringExtra("login_userEmail");
        key_value = getIntent().getStringExtra("key_value");
        user_login = getIntent().getStringExtra("login_userEmail");
        login_userName = getIntent().getStringExtra("login_userName");

        Log.d("Chat_App RecordActivity", "user_email:" + user_email + " key_value: " + key_value + " user_login:" + user_login);
        getSupportActionBar().setTitle(title_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        chat_list = new ArrayList<Getter_setter>();
        chat_list.clear();
        curr_user = FirebaseAuth.getInstance().getCurrentUser();
        editText = (EditText) findViewById(R.id.edittext);
        send_mess = (ImageButton) findViewById(R.id.send_mess);
        c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        currentdate = new java.text.SimpleDateFormat("MMM dd, yyyy");
        TodaysDate = currentdate.format(c.getTime());
        c = Calendar.getInstance();
        c.add(Calendar.HOUR, -hoursToSubtract);
        backdate = new java.text.SimpleDateFormat("MMM dd, yyyy");
        YesterdayDate = backdate.format(c.getTime());

        send_mess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editText.getText().toString().trim().isEmpty()) {
                    mess = editText.getText().toString().trim();
                    //       Toast.makeText(getApplicationContext(), "send mess", Toast.LENGTH_SHORT).show();
                    send_data_to_firebase(mess);
                }
            }
        });

        download_data_firebase();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                Log.d("Chat_App RecordActivity", "i1: " + i + " aBoolean: " + aBoolean);

                if (i >= 8 && aBoolean == true) {
                    swipeRefreshLayout.setRefreshing(true);
                    swipefn();
                }
            }
        });


    }
    private void swipefn() {
        Log.d("Chat_App RecordActivity", "swipefn timestamp: " + timestamp + " swipetimestamp: " + swipetimestamp);
        Log.d("Chat_App RecordActivity", "swipefn timestamp: " + timestamp + " swipetimestamp: " + swipetimestamp);

        chat_list.clear();
        timestamp = null;
        date = null;
        old_date = null;

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("Chats").child(curr_user.getUid()).child(key_value).orderByKey().addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //    Toast.makeText(RecordActivity.this, "swipe fn", Toast.LENGTH_SHORT).show();
                String key = dataSnapshot.getKey();
                Object value = dataSnapshot.getValue();
                Log.d("Chat_App RecordActivity", "swipefn onChildAdded dataSnapshot: " + dataSnapshot);
                Getter_setter getset = new Getter_setter();

                String key1 = dataSnapshot.getKey();
                Object value1 = dataSnapshot.getValue();

                if (timestamp == null || timestamp < Long.valueOf(key1)) {

                    timestamp = Long.valueOf(key1);

                    date_format(timestamp);

                    Long tsLong = System.currentTimeMillis();
                    final String ts = tsLong.toString();

                    Log.d("Chat_App RecordActivity", "swipefn onChildAdded key1: " + key1 + " value1: " + value1);
                    Log.d("Chat_App RecordActivity", "swipefn onChildAdded timestamp: " + timestamp);


                    if (((HashMap<String, String>) value1).containsKey("sent") && !((HashMap<String, String>) value1).get("sent").isEmpty()) {
                        send = ((HashMap<String, String>) value1).get("sent");
                        send_time = convert_to_time(key1);

                        if (((HashMap<String, String>) value1).containsKey("isRead") && !String.valueOf(((HashMap<String, String>) value1).get("isRead")).isEmpty() && !TextUtils.isEmpty(name)) {
                            statusofSendMsg = "seen";
                        } else {
                            statusofSendMsg = "not_seen";
                        }

                        getset.setSend(send);
                        getset.setSend_time(send_time);
                        getset.setStatusofSendMsg(statusofSendMsg);

                        Log.d("Chat_App RecordActivity", "swipefn onChildAdded send: " + send + " send_time: " + send_time);

                    } else if (((HashMap<String, String>) value1).containsKey("received") && !((HashMap<String, String>) value1).get("received").isEmpty()) {
                        received = ((HashMap<String, String>) value1).get("received");
                        received_time = convert_to_time(key1);

                        getset.setReceive(received);
                        getset.setReceive_time(received_time);

                        if (((HashMap<String, String>) value1).containsKey("isRead") && String.valueOf(((HashMap<String, String>) value1).get("isRead")).isEmpty()) {
                            databaseReference.child("Chats").child(curr_user.getUid()).child(key_value).child(String.valueOf(timestamp)).child("isRead").setValue(ts);// for sender update
                            databaseReference.child("Chats").child(key_value).child(curr_user.getUid()).child(String.valueOf(timestamp)).child("isRead").setValue(ts); // receiver update
                            }

                        Log.d("Chat_App RecordActivity", "swipefn onChildAdded received: " + received + " received_time: " + received_time);
                    }

                    if (TextUtils.isEmpty(date) || !date.equals(old_date)) {
                        getset.setDate(yourDate);
                        chat_list.add(getset);
                        old_date = date;
                    } else {

                        chat_list.add(getset);
                    }
                    adapter = new Record_adapter_class(ChatActivity.this, chat_list);
                    mMessageRecycler.setAdapter(adapter);
                    swipeRefreshLayout.setRefreshing(false);
                    aBoolean = false;

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getUrl(final HashMap<String, String> param1) {

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                String data = "";
                InputStream iStream = null;
                HttpURLConnection urlConnection = null;

                try {
                    Log.d("GM2..downloadUrl", "DownloadUrl Enterned");

                    URL url = new URL("https://fcm.googleapis.com/fcm/send");


                    // Creating an http connection to communicate with url
                    urlConnection = (HttpURLConnection) url.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    Log.d("GM2..download url1", "");
                    urlConnection.setRequestMethod("POST");


                    DataOutputStream dos = new DataOutputStream(urlConnection.getOutputStream());

                    dos.writeBytes("token=" + param1);
                    Log.d("Chat_App RecordActivity", "params1 dos: " + dos);
                    // Connecting to url
                    urlConnection.connect();
                    Log.d("GM2..download url2", "");


                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    }


                } catch (Exception e) {
                    Log.d("GM2..Execption", e.toString());
                } finally {
//                iStream.close();
                    urlConnection.disconnect();
                }
                Log.d("GM2..downloadUrl", "DownloadUrl Exit");
                //Your code goes here

            }
        });

        thread.start();
    }
    public String convert_to_time(String timestamp) {

        String updated_time;
        //     long tsLong =  Integer.parseInt(timestamp);

        long time = Long.parseLong(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        java.util.Date netDate = (new Date(time));
        updated_time = sdf.format(netDate);

        return updated_time;
    }

    public void send_data_to_firebase(final String mess) {

        Long tsLong = System.currentTimeMillis() / 1000;
        Long ls = tsLong * 1000;
        final String timestamp = ls.toString();
        Map<String, String> abc = ServerValue.TIMESTAMP;
        Log.d("Chat_App RecordActivity", "abc: " + ServerValue.TIMESTAMP);

        Map<String, Object> value = new HashMap<>();
        value.put("timestamp", ServerValue.TIMESTAMP);

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Chats").child("abc").setValue(ServerValue.TIMESTAMP);
             fn(mess);
    }

    public void fn(final String mess) {

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("Chats").child("abc").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.d("Chat_App RecordActivity", "abc2: " + snapshot);
                Object value = snapshot.getValue();
                server_timestamp = String.valueOf(value);

                Toast.makeText(ChatActivity.this, "single", Toast.LENGTH_SHORT).show();
                Log.d("Chat_App RecordActivity", "server_timestamp: " + server_timestamp);
                /*Toast.makeText(RecordActivity.this, "abc "+jj, Toast.LENGTH_SHORT).show();
                jj++;*/
                if (!TextUtils.isEmpty(server_timestamp) && server_timestamp != null) {

                    isRead_gettersetter record_getset = new isRead_gettersetter(mess, "");
                    ref.child("Chats").child(key_value).child(curr_user.getUid()).child(server_timestamp).setValue(record_getset);

                    //       ref.child("Chats").child(key_value).child(curr_user.getUid()).child(server_timestamp).child("received").setValue(mess);
                    //        ref.child("Chats").child(key_value).child(curr_user.getUid()).child(server_timestamp).child("isRead").setValue("");

                    ref.child("Chats").child(curr_user.getUid()).child(key_value).child(server_timestamp).child("sent").setValue(mess);
                    ref.child("Chats").child(curr_user.getUid()).child(key_value).child(server_timestamp).child("isRead").setValue("");


                    ref.child("Chats").child("abc").removeValue();
                    editText.setText("");

                    Long timestamp = Long.parseLong(server_timestamp);
                    date_format(timestamp);

                }// else
                //    Toast.makeText(RecordActivity.this, "Not working", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Log.d("Chat_App RecordActivity", "server_timestamp: " + server_timestamp);
        //   Toast.makeText(RecordActivity.this, "AAAAA ", Toast.LENGTH_SHORT).show();

        //    return server_timestamp;
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        download_data_firebase();
    }*/


   /* @Override
    protected void onStart() {
        super.onStart();
        download_data_firebase();
    }*/

    public void download_data_firebase() {

        databaseReference.child("Chats").child(curr_user.getUid()).child(key_value).limitToLast(8)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        chat_list.clear();
                        timestamp = null;
                        date = null;
                        old_date = null;
                        i = 0;


                        String key = dataSnapshot.getKey();
                        Object value = dataSnapshot.getValue();
                        Log.d("Chat_App RecordActivity", " onChildAdded dataSnapshot123: " + dataSnapshot);

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Getter_setter getset = new Getter_setter();
                            i++;
                            String key1 = snapshot.getKey();
                            Object value1 = snapshot.getValue();
                            //              if (timestamp == null || timestamp < Long.valueOf(key1)) {
                            timestamp = Long.valueOf(key1);
                            date_format(timestamp);


                            Log.d("Chat_App RecordActivity", " onChildAdded key1: " + key1 + " value1: " + value1);
                            Log.d("Chat_App RecordActivity", " onChildAdded timestamp: " + timestamp);


                            if (((HashMap<String, String>) value1).containsKey("sent") && !((HashMap<String, String>) value1).get("sent").isEmpty()) {
                                send = ((HashMap<String, String>) value1).get("sent");
                                send_time = convert_to_time(key1);


                                if (((HashMap<String, String>) value1).containsKey("isRead") && !String.valueOf(((HashMap<String, String>) value1).get("isRead")).isEmpty() && !TextUtils.isEmpty(name)) {
                                    statusofSendMsg = "seen";
                                } else {
                                    statusofSendMsg = "not_seen";
                                }
                                //          chat_list.add(getset);
                                getset.setSend(send);
                                getset.setSend_time(send_time);
                                getset.setStatusofSendMsg(statusofSendMsg);
                                lastMsg = send;
                                lastMsgTime = send_time;


                                Log.d("Chat_App RecordActivity", " onChildAdded send: " + send + " send_time: " + send_time + " statusofSendMsg: " + statusofSendMsg);

                            } else if (((HashMap<String, String>) value1).containsKey("received") && !((HashMap<String, String>) value1).get("received").isEmpty()) {
                                received = ((HashMap<String, String>) value1).get("received");
                                received_time = convert_to_time(key1);
                                Toast.makeText(ChatActivity.this, "recv", Toast.LENGTH_SHORT).show();

                                getset.setReceive(received);
                                getset.setReceive_time(received_time);
                                lastMsg = received;
                                lastMsgTime = received_time;


                                String lg = String.valueOf(((HashMap<String, String>) value1).get("isRead"));
                                Log.d("Chat_App RecordActivity", " onChildAdded lg1: " + lg);

                                if (((HashMap<String, String>) value1).containsKey("isRead") && String.valueOf(((HashMap<String, String>) value1).get("isRead")).isEmpty() && !TextUtils.isEmpty(name)) {
                                    Toast.makeText(ChatActivity.this, "update", Toast.LENGTH_SHORT).show();

                                    databaseReference.child("Chats").child(curr_user.getUid()).child(key_value).child(String.valueOf(timestamp)).child("isRead").setValue(ServerValue.TIMESTAMP);// for sender update
                                    databaseReference.child("Chats").child(key_value).child(curr_user.getUid()).child(String.valueOf(timestamp)).child("isRead").setValue(ServerValue.TIMESTAMP); // receiver update
                                    //                databaseReference.child("Chats").child(curr_user.getUid()).child(key_value).child(key1).child("isRead").setValue(ts);// for sender update
                                    //                databaseReference.child("Chats").child(key_value).child(curr_user.getUid()).child(key1).child("isRead").setValue(ts); // receiver update
                                    //        statusofSendMsg = "seen";

                                }

                                Log.d("Chat_App RecordActivity", " onChildAdded received: " + received + " received_time: " + received_time + " statusofSendMsg: " + statusofSendMsg);
                            }

                            if (TextUtils.isEmpty(date) || !date.equals(old_date)) {
                                getset.setDate(yourDate);
                                Log.d("Chat_App RecordActivity", " onChildAdded yourDate: " + yourDate + " date: " + date + " old_date: " + old_date);

                                chat_list.add(getset);


                                old_date = date;
                            } else {

                                chat_list.add(getset);
                            }
                            //             }

                            adapter = new Record_adapter_class(ChatActivity.this, chat_list);
                            mMessageRecycler.setAdapter(adapter);

                            aBoolean = true;


                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    public void date_format(Long timestamp) {

        date = new java.text.SimpleDateFormat("MMM dd, yyyy").format(new java.util.Date(timestamp));
        Log.d("123", " date: " + date);

        if (date.equals(TodaysDate))
            yourDate = "Today";
        else if (date.equals(YesterdayDate))
            yourDate = "Yesterday";
        else
            yourDate = date;

    }

    @Override
    public void onBackPressed() {
//        databaseReference.removeEventListener(childEventListener);

        super.onBackPressed();


        Intent a = new Intent(this, ContactActivity.class);
        //      a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        name = "";
        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        a.setFlags(a.FLAG_ACTIVITY_NEW_TASK | a.FLAG_ACTIVITY_CLEAR_TASK);


        a.putExtra("user_login", user_login);

        startActivity(a);
        finish();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("PRS reset_password", "onOptionsItemSelected: home btn  ");
                //       finish();
                name = "";

                //          databaseReference.removeEventListener(childEventListener);
                Intent i = new Intent(ChatActivity.this, ContactActivity.class);
                i.putExtra("user_login", user_login);

                // app icon in action bar clicked; go home
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
                //       i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                startActivity(i);
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }


    }
