package com.sword.yukti.nearby.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sword.yukti.nearby.ApiModel.PlaceJsonparser;
import com.sword.yukti.nearby.ContactActivity;
import com.sword.yukti.nearby.R;
import com.sword.yukti.nearby.Utility.Getter_setter;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.sword.yukti.nearby.Activity.LoginActivity.MyPREFERENCES;
import static com.sword.yukti.nearby.Activity.LoginActivity.USER_NAME;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener,View.OnClickListener
{
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    double mLatitude=0;
    double mLongitude=0;
    Marker marker;
    ArrayList<Getter_setter>mapsList;
    HashMap<String, String> mMarkerPlaceLink = new HashMap<String, String>();
    Button button,button1,chat;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionToggleButton;
    NavigationView navigationView;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    String usr_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.navigation_toolbar_parent);
        navigationView=(NavigationView)findViewById(R.id.parent_navigationview);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
       setSupportActionBar(toolbar);

        actionToggleButton = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open_parent, R.string.drawer_close_parent);
        drawerLayout.setDrawerListener(actionToggleButton);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);

        getSupportActionBar().setTitle("Dashboard");
        //method to keep the titlebar at center.
       // centerToolbarTitle(toolbar);
        prefs = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = prefs.edit();
       // usr_id = prefs.getString(USER_ID, 0);

        Log.d("login", String.valueOf(prefs.getString(USER_NAME,null)));

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
         button=(Button)findViewById(R.id.search_button);
         button1=(Button)findViewById(R.id.list_map);
         chat=(Button)findViewById(R.id.chat_button);
         button.setOnClickListener(this);
        button1.setOnClickListener(this);
        chat.setOnClickListener(this);
        navigationView .setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        drawerLayout.closeDrawer(navigationView);
                        break;
                    case R.id.feedback:

                        startActivity(new Intent(MapsActivity.this, Feedback.class));
                        finish();
                        break;
                    case R.id.settings:


                      //  startActivity( new Intent(MapsActivity.this, Settings.class));
                        //finish();
                        break;

                    case R.id.info:
                        startActivity( new Intent(MapsActivity.this, AppInfo.class));
                       // finish();
                        break;
                    case R.id.logout:
                        editor.remove("");
                        editor.clear();
                        editor.commit();
                        startActivity( new Intent(MapsActivity.this, LoginActivity.class));
                        finish();
                        break;


                }
                return true;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_button:
                Toast.makeText(this, "Nearby Restaurant", Toast.LENGTH_SHORT).show();
                Getter_setter getter_setter=new Getter_setter();
                mLatitude = mLastLocation.getLatitude();
                mLongitude = mLastLocation.getLongitude();
                getter_setter.setCurrentLatitude(mLatitude);
                getter_setter.setCurrentLongitude(mLongitude);
                LatLng latLng = new LatLng(mLatitude, mLongitude);
                marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                sb.append("location=" + mLatitude + "," + mLongitude);
                sb.append("&radius=3000");
                sb.append("&types=" +"restaurant");
                sb.append("&sensor=true");
                sb.append("&key=AIzaSyBFjK8UInAeNGfhx8attCH8UNY6xzNjuwU");
                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(sb.toString());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,MapsActivity.this);
        }

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_button:
                Toast.makeText(this, "Nearby hospital", Toast.LENGTH_SHORT).show();
                if(mLastLocation!= null){
                    mLatitude = mLastLocation.getLatitude();
                    mLongitude = mLastLocation.getLongitude();
                    LatLng latLng = new LatLng(mLatitude, mLongitude);
                    marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                    StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
                    sb.append("location=" + mLatitude + "," + mLongitude);
                    sb.append("&radius=4000");
                    sb.append("&types=" +"hospital");
                    sb.append("&sensor=true");
                    sb.append("&key=AIzaSyBFjK8UInAeNGfhx8attCH8UNY6xzNjuwU");
                    Log.d("sb:",sb.toString());
                    PlacesTask placesTask = new PlacesTask();
                    placesTask.execute(sb.toString());

                        button1.setVisibility(View.VISIBLE);

                }
                else
                {
                    Toast.makeText(this, "last location not found!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }


                break;
            case R.id.list_map:
                Intent intent=new Intent(MapsActivity.this,ListActivity.class);
                if (mapsList==null)
                {
                    Toast.makeText(this, "Please wait", Toast.LENGTH_SHORT).show();
                }
                else {
                    Collections.synchronizedList(mapsList);

                    intent.putExtra("Maps_list", mapsList);
                    startActivity(intent);
                }
                break;
            case R.id.chat_button:
                Intent intent_chat=new Intent(MapsActivity.this, ContactActivity.class);
                intent_chat.putExtra("user_login",prefs.getString(USER_NAME,null) );

                startActivity(intent_chat);
               break;

        }
    }
    double getDistanceFromLatLonInKm(double lat1,double lon1,double lat2,double lon2) {
        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double  dLon = deg2rad(lon2-lon1);
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        return d;
    }

    double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }


    private class PlacesTask extends AsyncTask<String, Integer, String>
    {
        String data = null;
        @Override
        protected String doInBackground(String... url)
        {
            try
            {
                data = downloadUrl(url[0]);
                Log.d("data:",data);
            }
            catch (Exception e)
            {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result)
        {
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
            Log.d("result",result);
        }

    }
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>>
    {
        JSONObject jObject;
        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData)
        {
            List<HashMap<String, String>> places = null;
            PlaceJsonparser placeJsonParser = new PlaceJsonparser();
            try
            {
                jObject = new JSONObject(jsonData[0]);
                places = placeJsonParser.parse(jObject);
            }
            catch (Exception e)
            {
                Log.d("Exception", e.toString());
            }
            return places;
        }
        @Override
        protected void onPostExecute(List<HashMap<String, String>> list)
        {


            mMap.clear();
            mapsList=new ArrayList<Getter_setter>();
            mapsList.clear();


            for (int i = 0; i < list.size(); i++)

            {   Getter_setter getter_setter=new Getter_setter();
                MarkerOptions markerOptions = new MarkerOptions();
                HashMap<String, String> hmPlace = list.get(i);
                double lat = Double.parseDouble(hmPlace.get("lat"));
                double lng = Double.parseDouble(hmPlace.get("lng"));
                String name = hmPlace.get("place_name");
                String vicinity = hmPlace.get("vicinity");
                String rating=hmPlace.get("rating");
                String refernce=hmPlace.get("reference");
                double distance= getDistanceFromLatLonInKm(mLatitude,mLongitude,lat,lng);
              //  Log.d("distance_in:",String.valueOf(distance));
              //  String photo=hmPlace.get("photo_reference");
                Log.d(rating,"rating");
                LatLng latLng = new LatLng(lat, lng);
                markerOptions.position(latLng);
                markerOptions.title(name + " : " + vicinity);
                getter_setter.setPlaceName(name);
                getter_setter.setVicinity(vicinity);
                getter_setter.setRating(rating);
                getter_setter.setReference(refernce);
                getter_setter.setDistance(distance);


                mapsList.add(getter_setter);
                Log.d("distance_in:",String.valueOf(getter_setter.getDistance()));
                Marker m = mMap.addMarker(markerOptions);
            //    Log.d("photo:",photo);
                mMarkerPlaceLink.put(m.getId(), hmPlace.get("reference"));
            }

        }
    }    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException
    {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try
        {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();


            urlConnection.connect();


            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null)
            {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }
        catch (Exception e)
        {
            Log.d("Exception while downloading url", e.toString());
        } finally
        {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


}