package com.sword.yukti.nearby.Activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.sword.yukti.nearby.Utility.Getter_setter;
import com.sword.yukti.nearby.ADapter.MapsListAdapter;
import com.sword.yukti.nearby.R;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
  public ArrayList<Getter_setter> list;
    MapsListAdapter adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        listView=(ListView)findViewById(R.id.list_view);
        list=new ArrayList<Getter_setter>();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1e90ff")));
        getSupportActionBar().setTitle("List of hospital");
        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.titlebar_color));
        }

       list=(ArrayList<Getter_setter>)getIntent().getSerializableExtra("Maps_list");
        adapter = new MapsListAdapter(ListActivity.this,  list);
        listView.setAdapter(adapter);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
