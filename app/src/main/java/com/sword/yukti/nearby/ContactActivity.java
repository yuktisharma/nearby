package com.sword.yukti.nearby;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sword.yukti.nearby.ADapter.Contact_adapter;
import com.sword.yukti.nearby.Activity.ChatActivity;
import com.sword.yukti.nearby.Activity.LoginActivity;
import com.sword.yukti.nearby.Utility.Getter_setter;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactActivity extends AppCompatActivity {
    ListView listView;
    Contact_adapter adapter;
    ArrayList<Getter_setter> listofUsers;
    String user_login;
    ArrayList<String> key_lis = new ArrayList<>();
    FirebaseAuth auth;
    DatabaseReference rootref=null,demoref=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        getSupportActionBar().setTitle("Contacts");

        listofUsers = new ArrayList<Getter_setter>();

        listView = (ListView) findViewById(R.id.lv);
        //       ArrayList<Getter_setter> listofUsers = (ArrayList<Getter_setter>) getIntent().getSerializableExtra("listofUsers");
        //      Log.d("list", String.valueOf(listofUsers.size()));
        user_login = getIntent().getStringExtra("user_login");
        Log.d("usser_login",user_login);
        //    Toast.makeText(getApplicationContext(), "Contact", Toast.LENGTH_SHORT).show();
        //    fn();
      /*  auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            // User is signed in (getCurrentUser() will be null if not signed in)
            Intent intent = Intent();
            startActivity(intent);
            finish();
        }*/listofUsers.clear();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Getter_setter getter_setter = listofUsers.get(position);
                // String key = key_lis.get(position);
                Intent i = new Intent(ContactActivity.this, ChatActivity.class);
               // i.putExtra("name", getter_setter.getName());
                i.putExtra("user_email", getter_setter.getEmail());
             //   i.putExtra("key_value", getter_setter.getKey_value());
                i.putExtra("user_login", user_login);

                Log.d("Chat_App ContactActivty", "user_email: " + getter_setter.getEmail() + " key_value: " + /*getter_setter.getKey_value() */ " user_login:" + user_login);

               // String key = getter_setter.getKey_value();
                //     Toast.makeText(getApplicationContext(),"Key :"+ key,Toast.LENGTH_SHORT).show();
             //   Log.d("Chat_App", "key: " + getter_setter.getKey_value());

                startActivity(i);
            }
        });
        fn();
    }

    @Override
    protected void onResume() {
        super.onResume();


        //      Toast.makeText(getApplicationContext(), "fn call", Toast.LENGTH_SHORT).show();

    }

    public void fn() {

       rootref = FirebaseDatabase.getInstance().getReference();
       demoref=rootref.child("nearby_table");
        Log.d("database_ref",String.valueOf(FirebaseDatabase.getInstance().getReference().child("nearby_table")));
        Log.d("key", String.valueOf(demoref));
        demoref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren())
                {   Getter_setter getset = new Getter_setter();

                    Object key=postSnapshot.getKey();
                    Object val = postSnapshot.getValue();
                    Log.d(String.valueOf(val), "val");
                    String user = ((HashMap<String, String>) val).get("email");
                    String number=((HashMap<String,String>)val).get("phone_number");
                    Log.d(user,"user");
                    Log.d(number,"phone_number");
                    if (!user_login.equalsIgnoreCase(user)){
                    getset.setFireBaseEmail(user);
                    getset.setFirebaseNumber(number);
                    getset.setFirebaseKey(String.valueOf(key));
                    // getset.setKey_value((String) key);
                    listofUsers.add(getset);}
                    //}

                }
                adapter = new Contact_adapter(ContactActivity.this, listofUsers);
                listView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


               /*     demoref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                             for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                 Log.d("snapy",String.valueOf(snap));
                            Getter_setter getset = new Getter_setter();

                            Object value = snap.getValue();
                            Object key = snap.getKey();
                            Log.d("Chat_App", "value: " + value + " key: " + key + "size: " + dataSnapshot.getChildrenCount());

                            String user = ((HashMap<String, String>) value).get("email");
                         //   String name = ((HashMap<String, String>) value).get("name");
                            Log.d("Chat_App",   user + " key: " + key);

                            if (!user_login.equals(user)) {

                              //  getset.setName(name);
                                getset.setEmail(user);
                               // getset.setKey_value((String) key);
                                listofUsers.add(getset);
                                //    key_lis.add((String) key1);
                                //       listofUser_Name.add(name);
                             //   Log.d("Chat_App", "name: " /*+ name *///+ //" user: " + user + " key: " + key);

                        /*    }


                        }
                        adapter = new Contact_adapter(ContactActivity.this, listofUsers);
                        listView.setAdapter((ListAdapter) adapter);

                        //             Log.d("Chat_App1", "name: " + getset.getName() + " email: " + getset.getEmail());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/
        //    Toast.makeText(getApplicationContext(), "Contact load load", Toast.LENGTH_SHORT).show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("PRS Contact Activity", "onCreateOptionsMenu Entered");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        //  return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("PRS Contact Activity", "onOptionsItemSelected Entered with case: " + item.getItemId());

        switch (item.getItemId()) {


            case R.id.logout:
                key_lis.clear();
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(ContactActivity.this, LoginActivity.class);
                i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }
    }

