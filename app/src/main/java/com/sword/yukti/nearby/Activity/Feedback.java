package com.sword.yukti.nearby.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sword.yukti.nearby.R;

public class Feedback extends AppCompatActivity {
    private FirebaseAuth mAuth;
    DatabaseReference rootRef = null, demoRef = null;
    EditText editText_input;
    Button feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        editText_input=(EditText)findViewById(R.id.editText);
        feedback=(Button)findViewById(R.id.feed_back);
        mAuth = FirebaseAuth.getInstance();
        rootRef=FirebaseDatabase.getInstance().getReference();
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentuser = mAuth.getCurrentUser().getUid();

                Log.d("uid",currentuser);
                demoRef = rootRef.child("nearby_table");
                demoRef.child(currentuser).child("Feedback").setValue(editText_input.getText().toString());
                startActivity(new Intent(Feedback.this,MapsActivity.class));

            }
        });




        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1e90ff")));
        getSupportActionBar().setTitle("List of hospital");
        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.titlebar_color));
        }

    }
}
