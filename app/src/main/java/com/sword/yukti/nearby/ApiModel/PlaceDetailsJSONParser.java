package com.sword.yukti.nearby.ApiModel;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by sword on 02-04-2018.
 */

public class PlaceDetailsJSONParser {
        public HashMap<String,String> parse(JSONObject jObject){

            JSONObject jPlaceDetails = null;
            try {
                /** Retrieves all the elements in the 'places' array */
                jPlaceDetails = jObject.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            /** Invoking getPlaces with the array of json object
             * where each json object represent a place
             */
            return getPlaceDetails(jPlaceDetails);
        }


        /** Parsing the Place Details Object object */
        private HashMap<String, String> getPlaceDetails(JSONObject jPlaceDetails){


            HashMap<String, String> hPlaceDetails = new HashMap<String, String>();

            String name = "-NA-";
            String icon = "-NA-";
            String vicinity="-NA-";
            String latitude="";
            String longitude="";
            String formatted_address="-NA-";
            String formatted_phone="-NA-";
            String website="-NA-";
            String rating="-NA-";
            String international_phone_number="-NA-";
            String url="-NA-";
            String photo_refernce="-NA-";
            try {
                // Extracting Place name, if available
                if(!jPlaceDetails.isNull("name")){
                    name = jPlaceDetails.getString("name");
                }

                // Extracting Icon, if available
                if(!jPlaceDetails.isNull("icon")){
                    icon = jPlaceDetails.getString("icon");
                }

                // Extracting Place Vicinity, if available
                if(!jPlaceDetails.isNull("vicinity")){
                    vicinity = jPlaceDetails.getString("vicinity");
                }

                // Extracting Place formatted_address, if available
                if(!jPlaceDetails.isNull("formatted_address")){
                    formatted_address = jPlaceDetails.getString("formatted_address");
                }

                // Extracting Place formatted_phone, if available
                if(!jPlaceDetails.isNull("formatted_phone_number")){
                    formatted_phone = jPlaceDetails.getString("formatted_phone_number");
                }

                // Extracting website, if available
                if(!jPlaceDetails.isNull("website")){
                    website = jPlaceDetails.getString("website");
                }

                // Extracting rating, if available
                if(!jPlaceDetails.isNull("rating")){
                    rating = jPlaceDetails.getString("rating");
                }

                // Extracting rating, if available
                if(!jPlaceDetails.isNull("international_phone_number")){
                    international_phone_number = jPlaceDetails.getString("international_phone_number");
                }

                // Extracting url, if available
                if(!jPlaceDetails.isNull("url")){
                    url = jPlaceDetails.getString("url");
                }

                latitude = jPlaceDetails.getJSONObject("geometry").getJSONObject("location").getString("lat");
                longitude = jPlaceDetails.getJSONObject("geometry").getJSONObject("location").getString("lng");
                if (!jPlaceDetails.isNull("photos")) {
                    JSONArray jarray = new JSONArray(jPlaceDetails.getString("photos"));
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jobj = jarray.optJSONObject(i);

                        photo_refernce = jobj.getString("photo_reference");

                    }
                }
                else {
                    photo_refernce="CmRaAAAAKGJzFRyt1iZNSrf7M_5m__o1PlL8wahpeJ7FEywvIVhi9_z_9DtIzVO1DRA7V5OStv_ZIzkBLlWX7xRj8G2vwJJGqt_JC2RjuYgZp1x-IF39sllS9Wm8K1d9ASSZy5yVEhAtAjjwSpCCPHxGJ8aG49UjGhQlU0kZjB-9PMfZYi483_rtUnTCaw";
                }
              //  photo_refernce=jPlaceDetails.getJSONObject("photos").getString("photo_reference");
                Log.d("photo_refernce:",photo_refernce);

                hPlaceDetails.put("name", name);
                hPlaceDetails.put("icon", icon);
                hPlaceDetails.put("vicinity", vicinity);
                hPlaceDetails.put("lat", latitude);
                hPlaceDetails.put("lng", longitude);
                hPlaceDetails.put("formatted_address", formatted_address);
                hPlaceDetails.put("formatted_phone", formatted_phone);
                hPlaceDetails.put("website", website);
                hPlaceDetails.put("rating", rating);
                hPlaceDetails.put("international_phone_number", international_phone_number);
                hPlaceDetails.put("url", url);
                hPlaceDetails.put("photo_reference",photo_refernce);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return hPlaceDetails;
        }
    }


