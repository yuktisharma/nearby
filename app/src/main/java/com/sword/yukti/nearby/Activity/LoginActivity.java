package com.sword.yukti.nearby.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sword.yukti.nearby.Databse.DBhelper;
import com.sword.yukti.nearby.R;
import com.sword.yukti.nearby.Utility.SessionDetails_GetSet;
import com.sword.yukti.nearby.Utility.SessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText login_id, password;
    Button signInButton;
    TextView forget_password, register_button;
    CheckBox rem_me;
    ProgressDialog loading;
    SessionManager sessionManager;
    SQLiteDatabase db;
    DBhelper dbHelper;
    private FirebaseAuth mAuth;
    SharedPreferences.Editor editor;
    FirebaseUser currentFirebaseUser;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String USER_NAME = "USER_NAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String USER_ID = "user_id";
    public static final String Key = "key_id";
    String usr_id = "";
    private static final int RC_SIGN_IN = 234;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dbHelper= new DBhelper(this);
        db = dbHelper.getReadableDatabase();

        sessionManager = new SessionManager(this);
        mAuth=FirebaseAuth.getInstance();


        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.titlebar_color));
        }

        login_id = (EditText) findViewById(R.id.signin_email);
        password = (EditText) findViewById(R.id.signin_pass);
        signInButton = (Button) findViewById(R.id.btnSignIn);
        forget_password = (TextView) findViewById(R.id.forget_pass);
        register_button = (TextView) findViewById(R.id.register);
        rem_me = (CheckBox) findViewById(R.id.checkBox);
     /*  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);*/

        Boolean savelogin = sessionManager.getUserLogginIn();
        if (!savelogin) {
            if (sessionManager.getLoginDetails()) {
                SessionDetails_GetSet sessionDetails_getSet = sessionManager.getUserDetails();
                login_id.setText(sessionDetails_getSet.getUsername());
                password.setText(sessionDetails_getSet.getPassword());
                rem_me.setChecked(true);
            }
            register_button.setOnClickListener(this);
        }

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.contains(USER_NAME) && sharedpreferences.contains(PASSWORD)) {
            startActivity(new Intent(LoginActivity.this, MapsActivity.class));
            finish();
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(LoginActivity.this, "Please Wait", null, true, true);
               final String emailInsert=login_id.getText().toString();
              final String  passwordInsert=password.getText().toString();
              if (emailInsert.equalsIgnoreCase("null")||emailInsert.equals("null")||emailInsert.isEmpty()||passwordInsert.isEmpty()||passwordInsert.equals("null")||passwordInsert.equalsIgnoreCase("null"))
                {
                    Toast.makeText(LoginActivity.this, "Please enter the required field", Toast.LENGTH_SHORT).show();
                }

                else{
                    mAuth.signInWithEmailAndPassword(emailInsert, passwordInsert).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d("PRS MainActivity", "login Task<AuthResult>" + String.valueOf(task));

                            if (!task.isSuccessful()) {
                                Toast.makeText(getApplication(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                loading.dismiss();
                                //       et_email.setText("");
                                //       et_passwd.setText("");
                            } else {
                                //       if (mAuth.getCurrentUser().isEmailVerified()) {
                                Log.d("isEmailVerified", "Email is verified.");


                                currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser(); // getting current user_id reference
                                Log.d("PRS MainActivity", "login currentFirebaseUser" + String.valueOf(currentFirebaseUser));

                                editor = sharedpreferences.edit();
                                editor.putString(USER_NAME, emailInsert);
                                editor.putString(PASSWORD, passwordInsert);
                                editor.putString(USER_ID, usr_id);
                                editor.commit();
                                String u_id = sharedpreferences.getString(USER_ID, "");
                                Log.d("PRS MainActivity", "login mail_pass_usr_id: " + emailInsert + passwordInsert + usr_id);
                                Log.d("PRS MainActivity", "login UIUUid: " + String.valueOf(currentFirebaseUser));
                                Log.d("PRS MainActivity", "login UIUUid:" + String.valueOf(currentFirebaseUser.getUid()));


                                Intent intent = new Intent(LoginActivity.this, MapsActivity.class);
                                // intent.putExtra("fullname",_fname);
                                intent.putExtra("email", emailInsert);
                                startActivity(intent);
                                //Removing MainActivity[Login Screen] from the stack for preventing back button press.
                                finish();
                            }
                        }
                    });

                }
                /*       cursor = db.rawQuery("SELECT *FROM User_info_details"+" WHERE "+Database.USER_EMAIL+"=? AND "+Database.USER_PASSWORD+"=?",new String[] {emailInsert,passwordInsert});
                if (cursor != null) {
                    if(cursor.getCount() > 0) {

                        cursor.moveToFirst();
                        //Retrieving User FullName and Email after successfull login and passing to LoginSucessActivity
                        //  String _fname = cursor.getString(cursor.getColumnIndex(Database.USER_NAME));
                        String _email = cursor.getString(cursor.getColumnIndex(Database.USER_EMAIL));
                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                    }    else {

                        //I am showing Alert Dialog Box here for alerting user about wrong credentials
                        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("Alert");
                        builder.setMessage("Username or Password is wrong.");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                        //-------Alert Dialog Code Snippet End Here
                    }
                }*/
            }

        });
        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String emailInsert=login_id.getText().toString();
                  if(emailInsert.equalsIgnoreCase("null")||emailInsert.equals("null")||emailInsert.isEmpty())
                  {
                      Toast.makeText(LoginActivity.this, "Please enter email address", Toast.LENGTH_SHORT).show();
                  } else{
                FirebaseAuth.getInstance().sendPasswordResetEmail(emailInsert)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LoginActivity.this, "Reset password is send to your email id:" +emailInsert, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });}
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.register:
                sessionManager.close_REquestPassword();
                ConnectivityManager Cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo ninfo = Cm.getActiveNetworkInfo();
                if (ninfo != null && ninfo.isConnected()) {
                    Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(registerIntent);
                }
                break;
            case R.id.btnSignIn:
                signInButton.setEnabled(true);
                Cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                assert Cm != null;
                ninfo = Cm.getActiveNetworkInfo();
                if (ninfo != null && ninfo.isConnected()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(login_id.getWindowToken(), 0);
                    }
                    String pass =password.getText().toString().trim();
                    String email = login_id.getText().toString().trim();

                    if (TextUtils.isEmpty(email) || TextUtils.isEmpty((pass))) {
                        signInButton.setEnabled(true);
                        Toast.makeText(LoginActivity.this, "Username or password can't be empty.", Toast.LENGTH_SHORT).show();
                    } else {


                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Network connection not available.", Toast.LENGTH_LONG).show();
                }
        }
    }
 /* @Override
    protected void onStart() {
        super.onStart();


        if (mAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, MapsActivity.class));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if the requestCode is the Google Sign In code that we defined at starting
        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("Nearby", "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("Nearby", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Toast.makeText(LoginActivity.this, "User Signed In", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Nearby", "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }*/
private void signIn() {
    //getting the google signin intent
    Intent signInIntent = mGoogleSignInClient.getSignInIntent();

    //starting the activity for result
    startActivityForResult(signInIntent, RC_SIGN_IN);
}


}
