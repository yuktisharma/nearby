package com.sword.yukti.nearby.Activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sword.yukti.nearby.Utility.Getter_setter;
import com.sword.yukti.nearby.ApiModel.PlaceDetailsJSONParser;
import com.sword.yukti.nearby.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class DescriptionActivity extends AppCompatActivity {
String hospital,address,refer;
TextView nameEntry,addressEntry;
RatingBar ratingBar;
WebView mWvPlaceDetails;
ImageView hospital_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        mWvPlaceDetails = (WebView) findViewById(R.id.wv_place_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        hospital_view=(ImageView)findViewById(R.id.imageView);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1e90ff")));
        getSupportActionBar().setTitle("hospital");
        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.titlebar_color));
        }


        mWvPlaceDetails.getSettings().setUseWideViewPort(false);

        nameEntry=(TextView)findViewById(R.id.Rest_name);
      //  addressEntry=(TextView)findViewById(R.id.hosp_add);
      //  ratingBar=(RatingBar)findViewById(R.id.ratings);
        hospital=getIntent().getStringExtra("place_name");
        address=getIntent().getStringExtra("place_address");
        refer=getIntent().getStringExtra("reference");
        Log.d(hospital,"hospital");
        Log.d(address,"address");
        Log.d(refer,"reference");
        nameEntry.setText(hospital);
       // addressEntry.setText(address);
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        sb.append("reference="+refer);
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyDryhOjbKmDzniqZQ1iINRXZ_9HRPYL4CQ");

   /*    if (Float.parseFloat(String.valueOf(rating))>1&&Float.parseFloat(String.valueOf(rating))<2)
        {
            ratingBar.setNumStars(1);
        }
        else if (Float.parseFloat(String.valueOf(rating))>2&&Float.parseFloat(String.valueOf(rating))<3)
        {
            ratingBar.setNumStars(2);
        }
        else if (Float.parseFloat(String.valueOf(rating))>3&&Float.parseFloat(String.valueOf(rating))<4)
        {
            ratingBar.setNumStars(3);
        }
        else if (Float.parseFloat(String.valueOf(rating))>4&& Float.parseFloat(String.valueOf(rating))<5)
        {
            ratingBar.setNumStars(4);
        }
        else
            {
                ratingBar.setNumStars(5);
            }

*/
        PlacesTask placesTask = new PlacesTask();

        // Invokes the "doInBackground()" method of the class PlaceTask
        placesTask.execute(sb.toString());
        Log.d("sb2:",sb.toString());
    }
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google place details in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
    }
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }


    /** A class to parse the Google Place Details in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, HashMap<String,String>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected HashMap<String,String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);
                JSONArray jsonRootArray = jObject.getJSONArray("results");
                for (int j = 0; j < jsonRootArray.length(); j++) {
                    JSONObject jobj = jsonRootArray.getJSONObject(j);
                    Log.d("ValueAtschoolChild", String.valueOf(jobj));
                    String icon = jobj.getString("icon");
                    // String vicinity = hPlaceDetails.get("vicinity");
                    String lat = jobj.getString("lat");
                    String lng = jobj.getString("lng");
                    String formatted_address = jobj.getString("formatted_address");
                    String formatted_phone = jobj.getString("formatted_phone");
                    String website = jobj.getString("website");
                    String rating = jobj.getString("rating");
                    String international_phone_number = hPlaceDetails.get("international_phone_number");
                    String url = jobj.getString("url");
                    JSONObject imageDetails_Object = jobj.getJSONObject("photos");
                    String link = imageDetails_Object.optString("photo_reference");
                    Getter_setter imageGetter = new Getter_setter();
                    imageGetter.setLat(lat);
                    imageGetter.setLongitude(lng);
                    imageGetter.setRating(rating);
                    imageGetter.setVicinity(formatted_address);
                    imageGetter.setUrl(url);
                    imageGetter.setWebsite(website);
                    imageGetter.setFormatted_phone(formatted_phone);
                    imageGetter.setPhoto_refernce(link);
                    Log.d("vicinity:",imageGetter.getVicinity());


                    // Start parsing Google place details in JSON format

                }
                Log.d(String.valueOf(hPlaceDetails), "place_detail");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            return hPlaceDetails;
        }
            // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(HashMap<String,String> hPlaceDetails){


         //   String name = hPlaceDetails.get("name");
            String icon = hPlaceDetails.get("icon");
           // String vicinity = hPlaceDetails.get("vicinity");
            String lat = hPlaceDetails.get("lat");
            String lng = hPlaceDetails.get("lng");
            String formatted_address = hPlaceDetails.get("formatted_address");
            String formatted_phone = hPlaceDetails.get("formatted_phone");
            String website = hPlaceDetails.get("website");
            String rating = hPlaceDetails.get("rating");
            String international_phone_number = hPlaceDetails.get("international_phone_number");
            String url = hPlaceDetails.get("url");
            String photo=hPlaceDetails.get("photo_reference");
            Log.d("photo_s",photo);
            if (photo!=null) {
                Picasso.with(DescriptionActivity.this).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&photoreference=" + photo + "&sensor=false&key=AIzaSyDryhOjbKmDzniqZQ1iINRXZ_9HRPYL4CQ").into(hospital_view);
            }String mimeType = "text/html";
            String encoding = "utf-8";
            Getter_setter imageGetter = new Getter_setter();
            String data = 	"<html>"+
                    "<br style='clear:both' />" +
                    "<hr  />"+
                    "<p>Vicinity : " + address + "</p>" +
                    //"<p>Location : " + lat + "," + lng + "</p>" +
                    "<p>Address : " + formatted_address + "</p>" +
                    "<p>Phone :  <a href='"+"tel:"+ formatted_phone +"'>"+formatted_phone+"</a>"+ "</p>" +
                    "<p>Rating : " + rating + "</p>" +
                    "<p>International Phone  :  <a href='"+"tel:"+ international_phone_number+"'>"+international_phone_number +"</a>"+ "</p>" +
                    "<p>Website :<a href='" + website+ "'>" + website +"</a>"+ "</p>" +

                    "<p>URL  : <a href='" + url + "'>" + url + "</p>" +
                    "</body></html>";


            // Setting the data in WebView
            mWvPlaceDetails.loadDataWithBaseURL("", data, mimeType, encoding, "");
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}

