package com.sword.yukti.nearby.Utility;


import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class Getter_setter implements Parcelable
{
    String email;
    String password;
    String address;
    String alternate_number;
    String rating;
    String confirm_password;
    String placeName;
    String vicinity;
    String reference;
    String icon;
    String lat;
    String longitude;
    String formatted_phone;
    String photo_refernce;
    String url;
    String website;
    Double currentLatitude;
    Double currentLongitude;
    String send;
    String receive;
    String send_time;
    String receive_time;

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    String bloodGroup;

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public String getReceive() {
        return receive;
    }

    public void setReceive(String receive) {
        this.receive = receive;
    }

    public String getSend_time() {
        return send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public String getReceive_time() {
        return receive_time;
    }

    public void setReceive_time(String receive_time) {
        this.receive_time = receive_time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatusofSendMsg() {
        return statusofSendMsg;
    }

    public void setStatusofSendMsg(String statusofSendMsg) {
        this.statusofSendMsg = statusofSendMsg;
    }

    String date;
    String statusofSendMsg;
    public String getFireBaseEmail() {
        return fireBaseEmail;
    }

    public void setFireBaseEmail(String fireBaseEmail) {
        this.fireBaseEmail = fireBaseEmail;
    }

    public String getFirebaseNumber() {
        return firebaseNumber;
    }

    public void setFirebaseNumber(String firebaseNumber) {
        this.firebaseNumber = firebaseNumber;
    }

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }

    Double hospitalLongitude;
    String fireBaseEmail;
    String firebaseNumber;
    String firebaseKey;

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    Double hospitalLatitude;
    Double distance;
    public Double getCurrentLatitude() {
        return currentLatitude;
    }

    public void setCurrentLatitude(Double currentLatitude)
    {
        this.currentLatitude = currentLatitude;
    }

    public Double getCurrentLongitude() {
        return currentLongitude;
    }

    public void setCurrentLongitude(Double currentLongitude) {
        this.currentLongitude = currentLongitude;
    }


    public String getFormatted_phone() {
        return formatted_phone;
    }

    public void setFormatted_phone(String formatted_phone) {
        this.formatted_phone = formatted_phone;
    }

    public String getPhoto_refernce() {
        return photo_refernce;
    }

    public void setPhoto_refernce(String photo_refernce) {
        this.photo_refernce = photo_refernce;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    protected Getter_setter(Parcel in) {
        email = in.readString();
        password = in.readString();
        address = in.readString();
        alternate_number = in.readString();
        confirm_password = in.readString();
        placeName = in.readString();
        vicinity = in.readString();
        rating=in.readString();
        reference=in.readString();
        distance=in.readDouble();
    }

    public static final Creator<Getter_setter> CREATOR = new Creator<Getter_setter>() {
        @Override
        public Getter_setter createFromParcel(Parcel in) {
            return new Getter_setter(in);
        }

        @Override
        public Getter_setter[] newArray(int size) {
            return new Getter_setter[size];
        }
    };

    public Getter_setter() {

    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }


    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlternate_number() {
        return alternate_number;
    }

    public void setAlternate_number(String alternate_number) {
        this.alternate_number = alternate_number;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(address);
        dest.writeString(alternate_number);
        dest.writeString(confirm_password);
        dest.writeString(placeName);
        dest.writeString(vicinity);
        dest.writeString(rating);
        dest.writeString(reference);
        dest.writeDouble(distance);
    }
}
