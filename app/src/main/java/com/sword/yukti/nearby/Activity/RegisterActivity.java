package com.sword.yukti.nearby.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sword.yukti.nearby.Databse.DBhelper;
import com.sword.yukti.nearby.R;
import com.sword.yukti.nearby.Utility.Getter_setter;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
EditText registerEmail,registerPassword,confirmPassword,registerAddress,alternatePhone;
 Spinner bloodGroup;
Button register;
String email,password,confirm_password,address,alternateNumber,bloodGrp;
DBhelper dBhelper;
ProgressDialog loading;
private FirebaseAuth mAuth;
DatabaseReference rootRef = null, demoRef = null;
String values[]= {"O+","A+","B+","O-","A-","B-","AB+","AB-"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        dBhelper=new DBhelper(this);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1e90ff")));
        getSupportActionBar().setTitle("Registeration");
        registerEmail=(EditText)findViewById(R.id.register_email);
        registerPassword=(EditText)findViewById(R.id.register_pass);
        confirmPassword=(EditText)findViewById(R.id.confirm_pass);
        registerAddress=(EditText)findViewById(R.id.address);
        alternatePhone=(EditText)findViewById(R.id.alternate_number);
        bloodGroup=(Spinner) findViewById(R.id.blood_group);
        register=(Button)findViewById(R.id.btnRegister);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        loading = new ProgressDialog(this);
        loading.setIndeterminate(true);
        loading.setCancelable(false);
        loading.setMessage("Registering...");
        rootRef = FirebaseDatabase.getInstance().getReference();
        Log.d("rootref:",String.valueOf(rootRef));
        bloodGroup.setAdapter(new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1,values));
        demoRef = rootRef.child("nearby_table");
        Log.d("demoref:",String.valueOf(demoRef));

        if (Build.VERSION.SDK_INT >= 21)
        {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.titlebar_color));
        }


        mAuth = FirebaseAuth.getInstance();

        bloodGroup.setDropDownHorizontalOffset(2);
        bloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bloodGrp=parent.getItemAtPosition(position).toString();
                Log.d("bloodgroup",bloodGrp);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                bloodGrp="null";
            }
        });

        register.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnRegister:
                Getter_setter getter_setter=new Getter_setter();
                dBhelper=new DBhelper(RegisterActivity.this);
                email=registerEmail.getText().toString().trim();
                password=registerPassword.getText().toString().trim();
                address=registerAddress.getText().toString().trim();
                confirm_password=confirmPassword.getText().toString().trim();
                alternateNumber=alternatePhone.getText().toString().trim();
                 getter_setter.setEmail(email);
                 getter_setter.setPassword(password);
                 getter_setter.setAddress(address);
                 getter_setter.setAlternate_number(alternateNumber);
                 getter_setter.setBloodGroup(bloodGrp);
                 if (email.isEmpty()||address.isEmpty()||password.isEmpty()||confirm_password.isEmpty()||alternateNumber.isEmpty())
                 {
                     Toast.makeText(this, "please filled the all required field", Toast.LENGTH_SHORT).show();
                 }
                 else {
                     if (confirm_password.equalsIgnoreCase(password)) {
                         dBhelper.addRegisterValues(getter_setter);
                      //   startActivity(loginIntent);
                         mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                             @Override
                             public void onComplete(@NonNull Task<AuthResult> task) {
                                 if (!task.isSuccessful()) {
                                     Toast.makeText(getApplicationContext(),  task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                     loading.dismiss();

                                     // Log.d("msg",task.getException().getMessage());
                                 } else {
                                     Toast.makeText(getApplicationContext(), " succesful !!!!!!!!", Toast.LENGTH_SHORT).show();

                                     verificationEmail();


                                 }

                             }
                         });

                     } else {
                         Toast.makeText(this, "confirmpassword and password is different", Toast.LENGTH_SHORT).show();
                     }
                 }
        }
    }
    private void verificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // email sent

                            email=registerEmail.getText().toString().trim();
                            password=registerPassword.getText().toString().trim();
                            address=registerAddress.getText().toString().trim();
                            confirm_password=confirmPassword.getText().toString().trim();
                            alternateNumber=alternatePhone.getText().toString().trim();
                            String currentuser = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            Log.d("uid",currentuser);
                            demoRef.child(currentuser).child("email").setValue(email);
                            demoRef.child(currentuser).child("address").setValue(address);
                            demoRef.child(currentuser).child("phone_number").setValue(alternateNumber);
                            demoRef.child(currentuser).child("password").setValue(password);
                            demoRef.child(currentuser).child("bloodGroup").setValue(bloodGrp);

                            // after email is sent just logout the user and finish this activity
                            FirebaseAuth.getInstance().signOut();

                            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                            i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.verification_toast), Toast.LENGTH_LONG).show();

                            // finish();
                        } else {
                            // email not sent, so display message and restart the activity or do whatever you wish to do

                            //restart this activity
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.errormsg), Toast.LENGTH_LONG).show();
                            overridePendingTransition(0, 0);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());

                        }
                    }
                });
        loading.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
