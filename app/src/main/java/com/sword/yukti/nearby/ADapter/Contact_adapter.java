package com.sword.yukti.nearby.ADapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sword.yukti.nearby.R;
import com.sword.yukti.nearby.Utility.Getter_setter;

import java.util.ArrayList;

/**
 * Created by sword on 09-05-2018.
 */

public class Contact_adapter extends BaseAdapter {
    Context context;
    ArrayList<Getter_setter> mylist = new ArrayList<>();
    ArrayList<Getter_setter>listofUsers;

    public Contact_adapter(Context context, ArrayList<Getter_setter> listofUsers) {
        this.context = context;
        this.listofUsers = listofUsers;
        this.mylist = new ArrayList<>();
        mylist.addAll(listofUsers);
    }

    @Override
    public int getCount() {
        return listofUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Getter_setter getter_setter = listofUsers.get(position);

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_list_row, null);
        }

      //  TextView tv_name = (TextView) convertView.findViewById(R.id.tv_name);
       // tv_name.setText(getter_setter.getName());
       // Log.d("name: ", getter_setter.getName());


        TextView tv_email = (TextView) convertView.findViewById(R.id.tv_email);
        tv_email.setText(getter_setter.getFireBaseEmail());

        return convertView;
    }
}


